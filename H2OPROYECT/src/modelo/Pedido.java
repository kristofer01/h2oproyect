/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Kristian
 */
public class Pedido {
    private String cliente;
    private int consePed;
    private char tipoPed;//true egreso, false ingreso
    private String fEnvio;
    private String hora;
    private String estado;
    private String valor;
    private String iva;

    public Pedido(int consePed,String cliente, char tipoPed, String fEnvio, String hora, String estado, String valor, String iva) {
        this.cliente = cliente;//
        this.consePed = consePed;//
        this.tipoPed = tipoPed;//
        this.fEnvio = fEnvio;//
        this.hora = hora;//
        this.estado = estado;//
        this.valor = valor;
        this.iva = iva;
    }

    @Override
    public String toString()
    {
        return ((tipoPed=='E')?"EGRESO":"INGRESO")+"Pedido: "+consePed+
                " de "+cliente+ " fecha de envio:" +fEnvio+
                " a las :" +hora + " estado: "+estado+ " valor: " +valor +"iva:"+iva;
    }
    
    public Pedido() {
    }

    public int getConsePed() {
        return consePed;
    }

    public void setConsePed(int consePed) {
        this.consePed = consePed;
    }

    public char getTipoPed() {
        return tipoPed;
    }

    public void setTipoPed(char tipoPed) {
        this.tipoPed = tipoPed;
    }

    public String getfEnvio() {
        return fEnvio;
    }

    public void setfEnvio(String fEnvio) {
        this.fEnvio = fEnvio;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getIva() {
        return iva;
    }

    public void setIva(String iva) {
        this.iva = iva;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }
}
