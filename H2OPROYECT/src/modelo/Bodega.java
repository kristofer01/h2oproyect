/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Kristian
 */
public class Bodega {
    private int lote;
    private String tipoProducto;
    private String fElab;
    private int cantidad;

    public Bodega() {
    }

    public Bodega(int lote, String tipoProducto, String fElab, int cantidad) {
        this.lote = lote;
        this.tipoProducto = tipoProducto;
        this.fElab = fElab;
        this.cantidad = cantidad;
    }

    public int getLote() {
        return lote;
    }

    public void setLote(int lote) {
        this.lote = lote;
    }

    public String getTipoProducto() {
        return tipoProducto;
    }

    public void setTipoProducto(String tipoProducto) {
        this.tipoProducto = tipoProducto;
    }

    public String getfElab() {
        return fElab;
    }

    public void setfElab(String fElab) {
        this.fElab = fElab;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    
    
    
}
