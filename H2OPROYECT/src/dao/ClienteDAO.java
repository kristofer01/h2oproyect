package dao;


import daoexception.ClienteNoValidoException;
import modelo.Cliente;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @authors
 * Kristian buitrago,
 * Daniel Sierra, 
 * Dairo Rivas
 */
public interface ClienteDAO {
    
     void  crearCli (Cliente cliente)throws ClienteNoValidoException;
     Cliente consultarClie(String id);
     boolean editarCli(Cliente cliente);
     Cliente[] clientesTotales();
   
}
