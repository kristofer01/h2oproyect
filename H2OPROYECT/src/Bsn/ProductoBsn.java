/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bsn;

import daoexception.ProductoNoValidoException;
import modelo.Producto;

/**
 *
 * @author Dan
 */
public interface ProductoBsn {
    boolean Agregar(Producto p);       
    Producto consultar(String tipolicor);
    boolean modificar(Producto pro);
    String[] consultartodo();
}
