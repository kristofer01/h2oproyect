/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bsn;

import modelo.Cliente;

/**
 *
 * @author Dan
 */
public interface ClienteBsn 
{
    void Crear(Cliente c);
    Cliente consultar(String id);
    boolean  modificar(Cliente c);
    Cliente[] inventarioTotal();
}
