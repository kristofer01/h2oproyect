/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bsn;

import modelo.Pedido;



/**
 *
 * @author Dan
 */
public interface PedidoBsn {
    /*digo que  en los dao si 
    no puede leer en el dao tira una ecxeption quedebe ser la
    misma que ya habramos definido , la de que no pudo
    leer el archivo .luego aqui en la capa del 
    negocio el catchea la eception y de ahi supongo que 
    tira la misma para ya el controlador..aqui no pueden 
    haber pedidos duplicados , eso no pasa.YA
    DEFINIR SI CONTROLAMOS EL EXITO O NO POR EXCEPTIONS O POR 
    RETURN ES MEJOR VERLO CUANDO IMPLEMENTAMOS cada METODO
    */
    void Agregar(Pedido p);
    Pedido consultar (String consecutivopedido);
    boolean modificar(Pedido p);
    Pedido[] consultarPedidos(String idcliente);
}
