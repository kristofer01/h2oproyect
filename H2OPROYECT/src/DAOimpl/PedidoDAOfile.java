/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOimpl;

import dao.PedidoDAO;
import daoexception.ClienteNoValidoException;
import daoexception.PedidoNoValidoException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Pedido;

/**
 *
 * @author Kristian
 */
public class PedidoDAOfile implements PedidoDAO {

    public static int consePed;

    public static final int lconsPed = 10;
    public static final int lFechEnv = 8;
    public static final int lHora = 6;
    public static final int lEstado = 15;
    public static final int lValor = 9;
    public static final int lIva = 10;
    
    private static File fichero;

    @Override
    public void Agregar(Pedido pedido) throws PedidoNoValidoException {
        if (!fichero.exists()) {
            fichero = new File("..\\Pedidos.txt");
            pedido.setConsePed(0);
        } else {
            consePed = conseActual(fichero);
            pedido.setConsePed(consePed);
        }
        if (validar(pedido)) {
            String s = convertirPedidoARegistro(pedido);
            try {
                BufferedWriter writer = new BufferedWriter(new FileWriter(fichero, true));
                writer.write(s);
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            throw new PedidoNoValidoException();
        }

    }

    public int conseActual(File tabla) {
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(tabla));
            String st;
            while ((st = br.readLine()) != null) {
                System.out.println("FILE WAS READ!");
            }
            String[] cargas = st.split(";");
            String[] cc = cargas[cargas.length - 1].split(",");
            return (Integer.parseInt(cc[0]) + 1);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return -1;
    }

    public boolean validar(Pedido pedido) {

        if (pedido.getCliente() == null || (pedido.getTipoPed() != 'I' && pedido.getTipoPed() != 'E') || pedido.getfEnvio() == null || pedido.getHora() == null || pedido.getEstado() == null || pedido.getValor() == null) {
            return false;
        }
        if ((pedido.getCliente().isEmpty() || pedido.getfEnvio().isEmpty() || pedido.getHora().isEmpty() || pedido.getEstado().isEmpty() || pedido.getValor().isEmpty())) {
            return false;
        }
        if (pedido.getCliente().length() > 10 || pedido.getfEnvio().length() > 8 || pedido.getHora().length() > 6 || pedido.getEstado().length() > 15 || pedido.getValor().length() > 9) {
            return false;
        };
        if (espacios(pedido.getCliente()) || espacios(pedido.getValor())) {
            return false;
        };

        return true;
    }

    public boolean espacios(String a) {
        boolean b = false;
        for (int i = 0; i < a.length(); i++) {
            if (a.charAt(i) == ' ') {
                b = true;
                return b;
            }
        }
        return b;
    }

    private String convertirPedidoARegistro(Pedido pedido) {
        StringBuilder pedidoString = new StringBuilder();
               
        pedidoString.append(rellenar(Integer.toString(pedido.getConsePed()), lconsPed)).append(",");
        pedidoString.append(rellenar(pedido.getCliente(), 10)).append(",");
        pedidoString.append(rellenar(Character.toString(pedido.getTipoPed()),1)).append(";");
        pedidoString.append(rellenar(pedido.getfEnvio(),lFechEnv)).append(";");
        pedidoString.append(rellenar(pedido.getHora(),lHora)).append(";");
         pedidoString.append(rellenar(pedido.getEstado(),lEstado)).append(";");
        pedidoString.append(rellenar(pedido.getValor(),lValor)).append(";");
        pedidoString.append(rellenar(pedido.getIva(),lIva)).append(";");
        return pedidoString.toString();
    }
    public String rellenar(String s, int l) {
        String g = s;         
        if (g.length() < l) {
           int faltan = l - g.length();
            for (int i =0; i < faltan; i++) {
                g += " ";
            }
        }
        return g;
    }

    @Override
    public Pedido consultar(String consecutivopedido) {
        if (!fichero.exists()) {
            return null;
        }

        try {
            BufferedReader br = new BufferedReader(new FileReader(fichero));
            String st;
            while ((st = br.readLine()) != null) {
                System.out.println(st);
            }
            String[] pedidoCaracteres = st.split(";");
            for (int i = 0; i < pedidoCaracteres.length; i++) {
                Pedido pedido = convertirCaracteresAPedido(pedidoCaracteres[i]);
                if (pedido.getConsePed() == Integer.parseInt(consecutivopedido)) {
                    return pedido;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    public boolean llaveDuplicada(File tabla, String key) {
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(tabla));
            String st;
            while ((st = br.readLine()) != null) {
                System.out.println("FILE WAS READ!");
            }
            String[] cargas = st.split(";");
            for (int i = 0; i < cargas.length; i++) {
                String[] cc = cargas[i].split(",");
                if (cc[0].equals(key)) {
                    return true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean modificar(Pedido pedido) {
        BufferedReader br = null;
        boolean b=false;
            try {
                StringBuilder pedidoEdit = new StringBuilder();
                br = new BufferedReader(new FileReader(fichero));
                String st;
                while ((st = br.readLine()) != null) {
                    System.out.println(st);
                }
                String[] pedidoCaracteres = st.split(";");
                for (int i = 0; i < pedidoCaracteres.length; i++) {
                    Pedido objPedido = convertirCaracteresAPedido(pedidoCaracteres[i]);
                    if (objPedido.getConsePed() == pedido.getConsePed()) {
                        pedidoEdit.append(pedido.getConsePed()).append(",");
                        pedidoEdit.append(pedido.getCliente()).append(",");                        
                        pedidoEdit.append(pedido.getTipoPed()).append(",");
                        pedidoEdit.append(pedido.getfEnvio()).append(",");
                        pedidoEdit.append(pedido.getHora()).append(",");
                        pedidoEdit.append(pedido.getEstado()).append(",");
                        pedidoEdit.append(pedido.getValor()).append(",");
                        pedidoEdit.append(pedido.getIva()).append(";");
                        b=true;
                    } else {
                        pedidoEdit.append(pedidoCaracteres[i]).append(";");
                    }
                }
                fichero.delete();
                fichero = new File("..\\Pedido.txt");
                try {
                    BufferedWriter writer = new BufferedWriter(new FileWriter(fichero, true));
                    writer.write(pedidoEdit.toString());
                    writer.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(PedidoDAOfile.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) { 
            Logger.getLogger(PedidoDAOfile.class.getName()).log(Level.SEVERE, null, ex);
        }
            return b;
        

        
    }

    private Pedido convertirCaracteresAPedido(String pedido) {
        String[] pedidoCaracteres = pedido.split(",");
        Pedido clienteObjeto = new Pedido(Integer.parseInt(eliminaEspa(pedidoCaracteres[0])),
                pedidoCaracteres[1],
                pedidoCaracteres[2].charAt(0),
                pedidoCaracteres[3],
                pedidoCaracteres[4],
                pedidoCaracteres[5],
                pedidoCaracteres[6],
                pedidoCaracteres[7]
        );
        return clienteObjeto;
    }
    private String eliminaEspa(String s){
        char[] var=s.toCharArray();
        String varNOespa="";
        int o=0;
        while(var[o]!=' '){
            varNOespa=varNOespa+var[o];
            o++;
        }
        return varNOespa;
    }

    
    @Override
    public Pedido[] consultarPedidos(String idcliente) {
       if (!fichero.exists()) {
            return null;
        }
       String[] pedidoCaracteres=null;
       String[] pedidosCliente=null;
       Pedido[] objPEDIDOS;
       String dt="";
        try {
            BufferedReader br = new BufferedReader(new FileReader(fichero));
            String st;
            
            while ((st = br.readLine()) != null) {
                System.out.println(st);
            }            
           pedidoCaracteres = st.split(";");
           String pedidosaux="";
           for(int i=0;i<pedidoCaracteres.length;i++){
               Pedido objPedido=convertirCaracteresAPedido(pedidoCaracteres[i]);
               if(eliminaEspa(objPedido.getCliente()).equals(idcliente)){
                   dt=dt+pedidoCaracteres[i]+";";
               }
           }
            pedidosCliente=dt.split(";");
            objPEDIDOS=new Pedido[pedidosCliente.length];
            for(int i=0;i<objPEDIDOS.length;i++){
            objPEDIDOS[i]=convertirCaracteresAPedido(pedidosCliente[i]);
            
            }
            return objPEDIDOS;
           
           
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null ;
        
    }

}
