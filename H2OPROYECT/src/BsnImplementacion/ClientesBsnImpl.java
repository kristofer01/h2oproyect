/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BsnImplementacion;

import Bsn.ClienteBsn;
import DAOimpl.ClienteDAOfile;
import dao.ClienteDAO;
import daoexception.ClienteNoValidoException;
import modelo.Cliente;

/**
 *
 * @author Kristian
 */
public class ClientesBsnImpl implements ClienteBsn{
ClienteDAO ClienteDAO;

    public ClientesBsnImpl() {
      ClienteDAO= new ClienteDAOfile();
    }
    @Override
    public void Crear(Cliente c){
        if(c.getId().equals(0000000000)){System.out.println("No se puede guardar por mal reporte de pago");
        }else{
        try{ClienteDAO.crearCli(c);
        }catch(ClienteNoValidoException  clienteNovalidoException){
            System.out.println("Cliente no valido por campos o llave repetida ");
        
        }
            
        }
      
    }
    @Override
    public Cliente consultar(String id) {
        
       if(ClienteDAO.consultarClie(id)!=null ){
           return ClienteDAO.consultarClie(id);
       }
       return null;
    }
    @Override
    public boolean modificar(Cliente c) {
    return ClienteDAO.editarCli(c);      
       
    }
    @Override
   public Cliente[] inventarioTotal(){
   return ClienteDAO.clientesTotales();
   }
}
