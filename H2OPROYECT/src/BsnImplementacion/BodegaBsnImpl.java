/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BsnImplementacion;

import Bsn.BodegaBsn;
import DAOimpl.BodegaDAOfile;
import dao.BodegaDAO;
import modelo.Bodega;
import modelo.Producto;

/**
 *
 * @author Kristian
 */
public class BodegaBsnImpl implements BodegaBsn {
    BodegaDAO bodegaDAO;

    public BodegaBsnImpl() {
        bodegaDAO= new BodegaDAOfile();
    }
    

    @Override
    public int consultar(String tpProducto) {
       return (bodegaDAO.consultar(tpProducto));
//        return (bodegaDAO.consultar(tpProducto));
//        }else{System.out.println("Aun no hay registros en el archivo");}
//        
    }

    @Override
    public boolean sacar(String tPlicor, int cuantos) {
       return bodegaDAO.sacar(tPlicor, cuantos);// mirar detalles con ddaniel el metodo de bodegaDAOFILE LE FALTA UN BOOLEANO
    
    }

    @Override
    public void entrar(Bodega b) {
        bodegaDAO.entrar(b);
    }

    @Override
    public Bodega[] consultarBodega() {
     return bodegaDAO.inventarioTotal();
    }
    
}
